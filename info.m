(* ::Package:: *)

(* ::Section::RGBColor[1, 1, 0]:: *)
(*INFO*)


BeginPackage["Info`"]

Info::usage =
        "Info package defines the following functions for creating and manipulating the auxiliary window to display the temporary information:
InfoStart  - adds a line to info window (creating it if necessary).
InfoEnd    - removes the last line from info window;
InfoPrint  - updates the last line;
InfoPrintT - updates the last line only if the last change was made more than 1 second ago;
InfoDelete - closes info window;
InfoButton - creates push button;
For more details see info for specific functions.";

InfoStart::usage = "InfoStart[SStrings___] puts a new line into the info window (creating it if necessary). The aruments SStrings___ can be strings or numbers (automatically converted to strings, similarly to effect of Print[] function). Supported options: WindowSize and WindowMargins";
InfoEnd::usage = "InfoEnd[] removes the last line from the info window.";
InfoPrint::usage= "InfoPrint[SStrings___] updates the last line in the info window. The aruments SStrings___ can be strings or numbers (automatically converted to strings, similarly to effect of Print[] function).";
InfoPrintT::usage= "InfoPrintT[SStrings___] conditionally updates the last line in the info window if the last change was made more than one second ago.";
InfoDelete::usage= "InfoDelete[] destroys the info window. Use option Enforce->True (deletes all windows entitled \"Info\") to kill the info window after restarting kernel.";
InfoButton::usage= "InfoButton[ttitle_,vvariable_] creates a button labelled ttitle and attaches the vvariable to it (by reference). Clicking the button sets vvariable=True";

Begin["Private`"]
Options[InfoStart]={WindowSize->{500,60},WindowMargins->{{Automatic,0},{Automatic,-40}},Background->Yellow,Magnification->0.5};
InfoStart[Shortest[SStrings___:"",1],OptionsPattern[]]:=Module[{},
\[FilledSquare]InfoNotebookLastModified\[FilledSquare]=AbsoluteTime[];
If[Count[Notebooks[],\[FilledSquare]InfoNotebook\[FilledSquare]]==0,
\[FilledSquare]InfoNotebook\[FilledSquare]=NotebookPut[Notebook[{},WindowTitle->"Info",WindowFloating->True,Saveable->False,WindowSize->OptionValue[WindowSize],WindowMargins->OptionValue[WindowMargins],ShowCellBracket->False,WindowElements->{},MenuPosition->None,WindowFrameElements->{"ResizeArea"},TrackCellChangeTimes->False,WindowToolbars->{},ScrollingOptions->{"VerticalScrollRange"->Fit},WindowClickSelect->False,Background->OptionValue[Background],Magnification->OptionValue[Magnification]],"CellInsertionPointCell"->Cell[]];
];
(*SetOptions[\[FilledSquare]InfoNotebook\[FilledSquare],"CellInsertionPointCell"\[Rule]Cell[]];*)
NotebookApply[\[FilledSquare]InfoNotebook\[FilledSquare],Cell[BoxData[StringJoin[Function[SString,If[StringQ[SString],SString,ToString[SString,TraditionalForm]]]/@List[SStrings]]],"Print",CellMargins->{{0,0},{0,0}}]];
];
(***********************************)
InfoEnd[]:=Module[{},
If[Count[Notebooks[],\[FilledSquare]InfoNotebook\[FilledSquare]]>0,
SelectionMove[\[FilledSquare]InfoNotebook\[FilledSquare],Previous,Cell];
NotebookDelete[\[FilledSquare]InfoNotebook\[FilledSquare]]]];
(***********************************)
Options[InfoDelete]={Enforce->False};
InfoDelete[OptionsPattern[]]:=Module[{},
If[Count[Notebooks[],\[FilledSquare]InfoNotebook\[FilledSquare]]>0,NotebookClose[\[FilledSquare]InfoNotebook\[FilledSquare]]];
If[OptionValue[Enforce],
While[Select[Notebooks[], (WindowTitle /. Options[#, WindowTitle]) == "Info" &]!={},
NotebookClose[Select[Notebooks[], (WindowTitle /. Options[#, WindowTitle]) == "Info" &][[1]]]
];
];
];
(***********************************)
InfoPrint[SStrings___]:=Module[{},
SelectionMove[\[FilledSquare]InfoNotebook\[FilledSquare],Previous,Cell];
NotebookDelete[\[FilledSquare]InfoNotebook\[FilledSquare]];
InfoStart[SStrings];
];
Options[InfoPrintT]={TimeInterval->1};
InfoPrintT[SStrings___]:=Module[{ttime=AbsoluteTime[]},
If[-\[FilledSquare]InfoNotebookLastModified\[FilledSquare]+ttime>OptionValue[TimeInterval],
\[FilledSquare]InfoNotebookLastModified\[FilledSquare]=ttime;
InfoPrint[SStrings];
];
]
(***********************************)
Attributes[InfoButton]=HoldAll;(*forces sending the parameters by reference*)
InfoButton[ttitle_,vvariable_(*sent by reference*)]:=Module[{},NotebookApply[\[FilledSquare]InfoNotebook\[FilledSquare],Cell[BoxData[FormBox[ButtonBox[ttitle,Appearance->Automatic,ButtonFunction:>(vvariable=True),Evaluator->Automatic,Method->"Preemptive"],TraditionalForm]],"Output"]]];
End[ ]

EndPackage[ ];


(* ::Section::RGBColor[1, 1, 0]:: *)
(*ASK*)


BeginPackage["Ask`"];
Ask::usage ="StopQ[QQuestion_:\"Continue evaluation?\",AAnswers_:{\"Yes\"\[Rule]True,\"No\"\[Rule]False}] - creates the dialog and aborts the evolution queue in the case of \"No\" answer";
StopQ::usage ="StopQ[QQuestion_:\"Continue evaluation?\",AAnswers_:{\"Yes\"\[Rule]True,\"No\"\[Rule]False}] - creates the dialog and aborts the evolution queue in the case of \"No\" answer";
Begin["Private`"];
StopQ[QQuestion_:"Continue evaluation?",AAnswers_:{"Yes"->True,"No"->False}]:=If[!ChoiceDialog[QQuestion,AAnswers],
FrontEndTokenExecute["EvaluatorAbort"];
Abort[];
,
True
];
End[];
EndPackage[];


(* ::Input:: *)
(**)
(**)
(*Get["E:\\My_documents\\BitBucket\\Superoperator\\info.m"]*)

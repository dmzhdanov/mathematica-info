Info package defines the functions for creating and manipulating the auxiliary separate window to display the temporary information (e.g., the calculation status). It provides the generic alternative to PrintTemporary and has the advantages of ease of monitoring the relevant parameters. 

The following functions are defined:

InfoStart  - adds a line to info window (after creating it, if necessary).

InfoEnd    - removes the last line from info window.

InfoPrint  - updates the last line.

InfoPrintT - updates the last line only if the last change was made more than 1 second ago (timing can be changed).

InfoDelete - closes info window.

InfoButton - creates push button.

For more details see info for specific functions (execute ?InfoStart etc.) or visit wiki page
https://bitbucket.org/dm_zhdanov/mathematica-info/wiki/Home

Check sample_usage.nb example as well.


